from django.db import models

class ProcessCategory(models.Model):
	name = models.CharField(max_length = 100, null = True)
	description = models.CharField(max_length = 500, null = True)
	is_deleted = models.BooleanField(default = False) 

class ProcessDetails(models.Model):
	process_category = models.ForeignKey(ProcessCategory,blank = True, null = True, on_delete=models.CASCADE)
	process_name = models.CharField(max_length = 100, blank = True, null = True) 
	is_deleted = models.BooleanField(default = False) 

class CsvData(models.Model):
	processName = models.CharField(max_length = 100)
	process_key = models.ForeignKey(ProcessDetails,blank = True, null = True, on_delete=models.CASCADE)
	processId = models.CharField(max_length = 100)
	resource_amount = models.CharField(max_length = 100)
	is_deleted = models.BooleanField(default = False) 
