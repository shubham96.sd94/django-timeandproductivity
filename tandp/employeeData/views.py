from django.shortcuts import render
import csv
from csvReader.models import CsvData
#from django.http import HttpResponse

# Create your views here.

def read_process_detail(request):
	with open(('./tandp/newtask.csv')) as f:
		reader = csv.reader(f)
		col1 = []
		col2 = []
		import pdb; pdb.set_trace()
		for row in reader:
			pass
			# col1.append(row[0])
			# col2.append(row[1])

	return "Hello"


def process_data(request):
	with open(('./tandp/csvDjango.csv')) as f:
		reader = csv.reader(f)
		next(reader)
		context={
		"name": "SIH"
		}
		for row in reader:
			# "12,321"
			str_res = row[4]
			res_final = ""
			for a_str_res in str_res:
				if a_str_res.isdigit():
					res_final += a_str_res
			_, created = CsvData.objects.get_or_create( processName = row[0], processId = row[1],resource_amount = res_final )
	return render(request,'employeeData/employee_data.html',context)

def view_chart(request):
	emp_data = CsvData.objects.all().order_by('-pk')
	all_memory_array = []
	all_process = []
	for a_emp_data in emp_data:
		if a_emp_data.processName not in all_process:
			all_memory_array.append(a_emp_data.resource_amount)
			all_process.append(a_emp_data.processName)

	all_data_obj = []
	# import pdb; pdb.set_trace()
	memory_arr = ",".join(all_memory_array)
	process_arr = ",".join(all_process)

	context = {
		"all_memory_array" : memory_arr ,
		"all_process" : process_arr 
	}
	return render(request,'employeeData/data.html',context)